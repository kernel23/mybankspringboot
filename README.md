# MyBankSpringBoot

J2EE web application that allows you to manage accounts belonging to customers. The application will manage two types of bank account, current and savings. Each account undergoes several operations, payment or withdrawal.
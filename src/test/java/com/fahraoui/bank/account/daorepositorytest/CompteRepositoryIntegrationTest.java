package com.fahraoui.bank.account.daorepositorytest;

import com.fahraoui.bank.account.daorepository.CompteRepository;
import com.fahraoui.bank.account.entities.Compte;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;

@RunWith(SpringRunner.class)
@SpringBootTest
@Transactional
public class CompteRepositoryIntegrationTest {
    @Autowired
    CompteRepository compteRepository;

    @Autowired
    EntityManager entityManager;

    @Test
    public void should_find_all_comptes() {
        List<Compte> compte = compteRepository.findAll();
        assertThat(compte).size();
    }

    @Test
    public void should_find_by_id() {
        Compte compte = compteRepository.getOne("c1");
        assertThat(compte).isNotNull();
        assertThat(compte.getSolde()).isGreaterThan(0);
    }

    @Test
    public void should_find_by_solde() {
        final List<Compte> compte = compteRepository.findBySoldeGreaterThan(0);
        assertThat(compte).size();
        assertThat(compte.get(0).getCodeCompte()).isEqualTo("c1");
    }

}

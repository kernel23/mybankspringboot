package com.fahraoui.bank.account.daorepositorytest;

import com.fahraoui.bank.account.daorepository.OperationRepository;
import com.fahraoui.bank.account.entities.Operation;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;

@RunWith(SpringRunner.class)
@SpringBootTest
@Transactional
public class OperationRepositoryIntegrationTest {
    @Autowired
    OperationRepository operationRepository;

    @Test
    public void should_find_all_comptes() {
        List<Operation> operations = operationRepository.findAll();
        assertThat(operations).size();
    }


}

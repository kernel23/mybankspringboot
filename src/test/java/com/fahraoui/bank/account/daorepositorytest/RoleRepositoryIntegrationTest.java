package com.fahraoui.bank.account.daorepositorytest;

import com.fahraoui.bank.account.daorepository.ClientRepository;
import com.fahraoui.bank.account.daorepository.RoleRepository;
import com.fahraoui.bank.account.entities.Role;
import com.fahraoui.bank.account.entities.RoleName;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import static org.assertj.core.api.Assertions.assertThat;

@RunWith(SpringRunner.class)
@SpringBootTest
public class RoleRepositoryIntegrationTest {

    @Autowired
    private RoleRepository roleRepository;
    @Autowired
    private ClientRepository clientRepository;

    @Test
    public void should_find_Roles_by_name() {
        Role roles = roleRepository.findByName((RoleName.ROLE_ADMIN)).get();
        assertThat(roles).isNotNull();
    }

}

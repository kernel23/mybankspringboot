package com.fahraoui.bank.account.daorepositorytest;

import com.fahraoui.bank.account.daorepository.ClientRepository;
import com.fahraoui.bank.account.entities.Client;
import org.hamcrest.MatcherAssert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.greaterThanOrEqualTo;
import static org.hamcrest.Matchers.is;

@RunWith(SpringRunner.class)
@SpringBootTest
@Transactional
public class ClientRepositoryIntegrationTest {

    @Autowired
    ClientRepository clientRepository;
    @Autowired
    EntityManager entityManager;

    @Test
    public void should_find_all_clients() {
        List<Client> client = clientRepository.findAll();
        assertThat(client).size();
    }

    @Test
    public void should_find_by_id() {
        Client client = clientRepository.getOne(2L);
        assertThat(client).isNotNull();
        assertThat(client.getName()).containsIgnoringCase("zack");
    }

    @Test
    public void should_find_all_users() {
        List<Client> users = clientRepository.findAll();
        MatcherAssert.assertThat(users.size(), is(greaterThanOrEqualTo(0)));
    }

    @Test
    public void should_find_by_username() {
        Client users = clientRepository.findByUsername("admin").get();
        assertThat(users).isNotNull();
    }
}

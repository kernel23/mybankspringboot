package com.fahraoui.bank.account.metierservicestest;

import com.fahraoui.bank.account.entities.Client;
import com.fahraoui.bank.account.entities.CompteCourant;
import com.fahraoui.bank.account.entities.CompteEpargne;
import com.fahraoui.bank.account.metierservices.BankMetierService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class BankMetierServiceTest {

    protected BankMetierService bankMetierService;

    @BeforeEach
    protected void setup() {
        bankMetierService = mock(BankMetierService.class);
    }

    @Test
    void should_Client_empty_after_init() {
        given(bankMetierService.getClients()).willReturn(new ArrayList<>());
        assertTrue(bankMetierService.getClients().isEmpty());
    }

    @Test
    void should_Comptes_empty_after_init() {
        given(bankMetierService.getComptes()).willReturn(new ArrayList<>());
        assertTrue(bankMetierService.getComptes().isEmpty());
    }

    @Test
    void should_return_all_clients() {
        ArrayList<Client> clients = new ArrayList<>();
        clients.add(new Client());
        clients.add(new Client());
        clients.add(new Client());

        given(bankMetierService.getClients()).willReturn(clients);
        assertFalse(clients.isEmpty());
        assertEquals(3, bankMetierService.getClients().size());
    }


    @Test
    void should_return_client_with_id() {
        Client client = new Client();
        given(bankMetierService.getClientById(1L)).willReturn(Optional.of(client));
        assertNotNull(bankMetierService.getClientById(1L));
    }

    @Test
    void should_return_compte_with_id() {
        CompteCourant compteCourant = new CompteCourant();
        CompteEpargne compteEpargne = new CompteEpargne();
        given(bankMetierService.getCompteById("CC1")).willReturn(Optional.of(compteCourant));
        assertNotNull(bankMetierService.getCompteById("CC1"));
        given(bankMetierService.getCompteById("CE2")).willReturn(Optional.of(compteEpargne));
        assertNotNull(bankMetierService.getCompteById("CE2"));
    }

    @Test
    void should_return_client_with_username() {
        Client client = new Client();
        client.setUsername("test1");
        when(bankMetierService.getCLientByUsername(any())).thenReturn(List.of());
        assertEquals(List.of(), bankMetierService.getCLientByUsername(client.getUsername()));
    }

    @Test
    void should_delete_throw_exception_client() {
        Client client = new Client();
        client.setId(1L);
        when(bankMetierService.deleteClients(eq(-1L))).thenThrow(new RuntimeException("error id"));
    }

    @Test
    void should_delete_throw_exception_compte() {
        CompteCourant compteCourant = new CompteCourant();
        CompteEpargne compteEpargne = new CompteEpargne();
        compteCourant.setCodeCompte("C1");
        compteEpargne.setCodeCompte("C2");
        when(bankMetierService.deleteCompte(eq("C1"))).thenThrow(new RuntimeException("error id"));
        when(bankMetierService.deleteCompte(eq("C2"))).thenThrow(new RuntimeException("error id"));
    }

    @Test
    void should_delete_throw_exception_compte_() {
        CompteCourant compteCourant = new CompteCourant();
        CompteEpargne compteEpargne = new CompteEpargne();
        compteCourant.setCodeCompte("C1");
        compteEpargne.setCodeCompte("C2");
        when(bankMetierService.deleteCompte(eq("C1"))).thenThrow(new RuntimeException("error id"));
        when(bankMetierService.deleteCompte(eq("C2"))).thenThrow(new RuntimeException("error id"));
    }

    @Test
    void should_create_compte_courant_and_epargne_and_returns_msg() {
        CompteCourant compteCourant = new CompteCourant();
        CompteEpargne compteEpargne = new CompteEpargne();
        compteCourant.setCodeCompte("CC1");
        compteEpargne.setCodeCompte("CE2");

        when(bankMetierService.addCompteCourant(any())).thenReturn(String.valueOf(compteCourant));
        when(bankMetierService.addCompteEpargne(any())).thenReturn(String.valueOf(compteEpargne));
        assertEquals("CC1", compteCourant.getCodeCompte());
        assertEquals("CE2", compteEpargne.getCodeCompte());
    }

    @Test
    void should_add_operations_verser_withdrawal_transfer() {
        given(bankMetierService.verser("C100", 300)).willReturn(any());
        assertEquals(any(), bankMetierService.verser("C100", 300));
        given(bankMetierService.withdrawal("C100", 300)).willReturn(any());
        assertEquals(any(), bankMetierService.verser("C100", 300));
        given(bankMetierService.transfer("C1", "C2", 300)).willReturn(any());
        assertEquals(any(), bankMetierService.verser("C100", 300));
    }
}

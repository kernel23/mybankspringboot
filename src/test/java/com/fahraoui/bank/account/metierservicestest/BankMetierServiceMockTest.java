package com.fahraoui.bank.account.metierservicestest;

import com.fahraoui.bank.account.daorepository.ClientRepository;
import com.fahraoui.bank.account.daorepository.CompteRepository;
import com.fahraoui.bank.account.daorepository.OperationRepository;
import com.fahraoui.bank.account.entities.Client;
import com.fahraoui.bank.account.entities.CompteCourant;
import com.fahraoui.bank.account.entities.CompteEpargne;
import com.fahraoui.bank.account.exception.ResourceNotFoundException;
import com.fahraoui.bank.account.metierservices.BankMetierService;
import org.junit.Test;
import org.junit.jupiter.api.BeforeEach;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.junit.MockitoJUnitRunner;

import java.util.Date;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class BankMetierServiceMockTest {

    @InjectMocks
    private BankMetierService bankMetierService;

    @Mock
    private ClientRepository clientRepository;
    @Mock
    private CompteRepository compteRepository;
    @Mock
    private OperationRepository operationRepository;


    @BeforeEach
    public void setup() throws ResourceNotFoundException {
        MockitoAnnotations.initMocks(this);
    }


    @Test
    public void should_get_clients_nominal() {
        bankMetierService.getClients();
        verify(clientRepository).findAll();
    }

    @Test
    public void should_get_comptes_nominal() {
        Client client1 = new Client("zakaria", "fahraoui@hotmail.fr", "user1", "user123");
        Client client2 = new Client("test2", "test2@hotmail.fr", "user1", "user123");
        when(compteRepository.findAll()).thenReturn(Stream
                .of(new CompteEpargne("c9", new Date(), 90000.0, client1, 5.5),
                        new CompteCourant("c10", new Date(), 6000.0, client2, 60000)).collect(Collectors.toList()));
        assertEquals(2, bankMetierService.getComptes().size());
    }

//    @Test
//    public void should_get_compteById_nominal() {
//        Client client1 = new Client("zakaria", "fahraoui@hotmail.fr", "user1", "user123");
//        Client client2 = new Client("test2", "test2@hotmail.fr", "user1", "user123");
//        Compte compteCourant = new CompteCourant("c10", new Date(), 6000.0, client2, 60000);
//        Compte compteEpargne = new CompteEpargne("c9", new Date(), 90000.0, client1, 5.5);
//
//        bankMetierService.getCompteById(compteEpargne.getCodeCompte());
//        bankMetierService.getCompteById(compteCourant.getCodeCompte());
//        MatcherAssert.assertThat(compteEpargne.getClient().getName(), is("zakaria"));
//        MatcherAssert.assertThat(compteCourant.getClient().getName(), is("test2"));
//    }
//
//    @Test
//    public void should_get_clientById_nominal() {
//        Client client1 = new Client("zakaria", "fahraoui@hotmail.fr", "user1", "user123");
//        bankMetierService.getClientById(client1.getId());
//        MatcherAssert.assertThat(client1.getName(), is("zakaria"));
//    }


}

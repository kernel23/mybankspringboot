package com.fahraoui.bank.account.webcontrollers;

import com.fahraoui.bank.account.entities.*;
import com.fahraoui.bank.account.exception.ResourceNotFoundException;
import com.fahraoui.bank.account.metierservices.IBankMetierService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.client.RestTemplate;

import java.util.List;
import java.util.Map;

/**
 * @author Zakaria FAHRAOUI <zakaria.fahraoui@gmail.com>
 * @Created 03/04/2020 05:10 AM.
 */

@CrossOrigin("*")
@RestController
@RequestMapping("/api/bank")
@RequiredArgsConstructor
public class BankController {
    private final RestTemplate restTemplate;
    private final IBankMetierService bankMetierService;

    @GetMapping(path = "/clients")
    @PreAuthorize("hasRole('USER') or hasRole('ADMIN')")
    public List<Client> getAllClients() {
        return bankMetierService.getClients();
    }

    @GetMapping(path = "/clients/{id}")
    @PreAuthorize("hasRole('USER') or hasRole('ADMIN')")
    public ResponseEntity<Client> getClientsId(@PathVariable("id") Long id) throws ResourceNotFoundException {
        Client client;
        client = bankMetierService.getClientById(id).orElseThrow(() -> new ResourceNotFoundException("Client not found for this id : " + id));
        return ResponseEntity.ok().body(client);
    }

    @GetMapping(path = "/comptes")
    @PreAuthorize("hasRole('USER') or hasRole('ADMIN')")
    public List<Compte> getAllComptes() {
        return bankMetierService.getComptes();
    }

    @GetMapping(path = "/comptes/{id}")
    @PreAuthorize("hasRole('USER') or hasRole('ADMIN')")
    public ResponseEntity<Compte> getComptesId(@PathVariable("id") String id) throws ResourceNotFoundException {
        Compte compte;
        compte = bankMetierService.getCompteById(id).orElseThrow(() -> new ResourceNotFoundException("Compte not found for this id :: " + id));
        return ResponseEntity.ok().body(compte);
    }

    @DeleteMapping(path = "/comptes/{id}")
    @PreAuthorize("hasRole('ADMIN')")
    public String deleteCompte(@PathVariable String id) {
        return bankMetierService.deleteCompte(id);
    }

    @DeleteMapping("/clients/{clientId}")
    @PreAuthorize("hasRole('ADMIN')")
    public String deleteClients(@PathVariable Long clientId) {
        return bankMetierService.deleteClients(clientId);
    }

    @PostMapping(path = "/compteCourant")
    public String addCompteCourant(@RequestBody CompteCourant compteCourant) {
        return bankMetierService.addCompteCourant(compteCourant);
    }

    @PostMapping(path = "/compteEpargne")
    public String addCompteEpargne(@RequestBody CompteEpargne compteEpargne) {
        return bankMetierService.addCompteEpargne(compteEpargne);
    }

    @PostMapping("/comptes/{id}/versOperation")
    @PreAuthorize("hasRole('ADMIN')")
    public String VersOperation(@PathVariable("id") String id, @RequestBody Versement montantV) {
        return bankMetierService.verser(id, montantV.getMontant());
    }

    @PostMapping("/comptes/{id}/retOperation")
    @PreAuthorize("hasRole('ADMIN')")
    public String retraitOperation(@PathVariable("id") String id, @RequestBody Retrait montantV) {
        return bankMetierService.withdrawal(id, montantV.getMontant());
    }

    @PostMapping("/comptes/{id}/versementOperation")
    @PreAuthorize("hasRole('ADMIN')")
    public String versementOperation(@PathVariable("id") String id, @RequestBody Map<String, String> params) {
        return bankMetierService.transfer(id, params.get("codeCompte"), Double.parseDouble(params.get("montant")));
    }

    @GetMapping(value = "/forex")
    @PreAuthorize("hasRole('USER') or hasRole('ADMIN')")
    public String getListForex() {
        String url = "https://www.freeforexapi.com/api/live";
        ResponseEntity<String> responseEntity = restTemplate.exchange(url, HttpMethod.GET, null, String.class);

        return responseEntity.getBody();
    }

    @GetMapping(value = "/forex/{id}")
    @PreAuthorize("hasRole('USER') or hasRole('ADMIN')")
    public String getForexAuChoix(@PathVariable("id") String id) {
        String url = "https://www.freeforexapi.com/api/live?pairs=" + id;
        ResponseEntity<String> responseEntity = restTemplate.exchange(url, HttpMethod.GET, null, String.class);

        return responseEntity.getBody();
    }

    @GetMapping(value = "/operations/comptes/{id}")
    @PreAuthorize("hasRole('USER') or hasRole('ADMIN')")
    public List<Operation> getOperationByCompte(@PathVariable("id") String id) {
        return bankMetierService.listOperation(id);
    }

    @GetMapping(value = "/operations")
    @PreAuthorize("hasRole('USER') or hasRole('ADMIN')")
    public List<Operation> getClientByUsername(@RequestParam(required = false) String username) {
        return bankMetierService.getCLientByUsername(username);
    }
}

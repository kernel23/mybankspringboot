package com.fahraoui.bank.account.daorepository;

import com.fahraoui.bank.account.entities.Role;
import com.fahraoui.bank.account.entities.RoleName;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;


@Repository
public interface RoleRepository extends JpaRepository<Role, Long> {
    Optional<Role> findByName(RoleName roleName);
}

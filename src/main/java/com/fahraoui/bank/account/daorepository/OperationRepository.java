package com.fahraoui.bank.account.daorepository;

import com.fahraoui.bank.account.entities.Operation;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface OperationRepository extends JpaRepository<Operation, Long> {
    @Query("select o from Operation o where o.compte.codeCompte=:x order by  o.dateCreation desc ")
    List<Operation> listOperationByCompte(@Param("x") String codeCpte);

    @Query("select o,c,u from Operation o,Compte c, Client u where o.compte.codeCompte= c.codeCompte and c.client.id = u.id and u.username=:x")
    List<Operation> listOperationByUser(@Param("x") String userName);
}

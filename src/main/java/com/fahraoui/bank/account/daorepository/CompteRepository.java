package com.fahraoui.bank.account.daorepository;

import com.fahraoui.bank.account.entities.Compte;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface CompteRepository extends JpaRepository<Compte, String> {
    List<Compte> findBySoldeGreaterThan(double solde); // for testing
}

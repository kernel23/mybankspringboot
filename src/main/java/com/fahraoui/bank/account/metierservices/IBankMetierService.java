package com.fahraoui.bank.account.metierservices;

import com.fahraoui.bank.account.entities.*;

import java.util.List;
import java.util.Optional;

public interface IBankMetierService {

    List<Client> getClients();

    Optional<Client> getClientById(Long id);

    List<Operation> getCLientByUsername(String username);

    List<Compte> getComptes();

    Optional<Compte> getCompteById(String codeCpte);

    String deleteCompte(String id);

    String deleteClients(Long clientId);

    String addCompteCourant(CompteCourant compteCourant);

    String addCompteEpargne(CompteEpargne compteEpargne);

    String verser(String codeCpte, double montant);

    String withdrawal(String codeCpte, double montant);

    String transfer(String codeCpte1, String codeCpte2, double montant);

    List<Operation> listOperation(String codeCpte);
}

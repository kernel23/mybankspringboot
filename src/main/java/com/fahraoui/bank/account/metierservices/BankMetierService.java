package com.fahraoui.bank.account.metierservices;

import com.fahraoui.bank.account.daorepository.ClientRepository;
import com.fahraoui.bank.account.daorepository.CompteRepository;
import com.fahraoui.bank.account.daorepository.OperationRepository;
import com.fahraoui.bank.account.entities.*;
import com.fahraoui.bank.account.exception.ResourceNotFoundException;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Date;
import java.util.List;
import java.util.Optional;

@Service
@RequiredArgsConstructor
public class BankMetierService implements IBankMetierService {

    private final ClientRepository clientRepository;
    private final CompteRepository compteRepository;
    private final OperationRepository operationRepository;

    @Transactional(readOnly = true)
    @Override
    public List<Client> getClients() {
        return clientRepository.findAll();
    }

    @Transactional
    @Override
    public Optional<Client> getClientById(Long id) {
        Optional<Client> client = clientRepository.findById(id);
        if (client.isEmpty()) throw new RuntimeException("Client Introuvable!");
        return client;
    }

    @Transactional
    @Override
    public List<Operation> getCLientByUsername(String userName) {
        Optional<Client> client = clientRepository.findByUsername(userName);
        if (client.isEmpty()) throw new RuntimeException("Client Introuvable!");
        return operationRepository.listOperationByUser(userName);
        //return operationRepository.listUserByOperation(username);
    }

    @Transactional(readOnly = true)
    @Override
    public List<Compte> getComptes() {
        return compteRepository.findAll();
    }

    @Transactional
    @Override
    public Optional<Compte> getCompteById(String codeCpte) {
        Optional<Compte> cp = compteRepository.findById(codeCpte);
        if (cp.isEmpty()) throw new RuntimeException("Compte Introuvable!");
        return cp;
    }

    @Transactional
    @Override
    public String deleteCompte(String id) {
        return compteRepository.findById(id)
                .map(compte -> {
                    compteRepository.delete(compte);
                    return "Delete Successfully!";
                }).orElseThrow(() -> new ResourceNotFoundException("Compte not found with id " + id));
    }

    @Transactional
    @Override
    public String deleteClients(Long clientId) {

        if (!clientRepository.existsById(clientId)) {
            throw new ResourceNotFoundException("client not found!");
        }
        return clientRepository.findById(clientId)
                .map(client -> {
                    clientRepository.delete(client);
                    return "Deleted Successfully!";
                }).orElseThrow(() -> new ResourceNotFoundException("Client not found!"));
    }

    @Transactional
    @Override
    public String addCompteCourant(CompteCourant compteCourant) {
        if (compteRepository.findById(compteCourant.getCodeCompte()).isPresent()) {
            throw new RuntimeException("Compte deja existant");
        } else {
            compteRepository.save(compteCourant);
            return "Votre Compte Courant bien ajouter";
        }
    }

    @Transactional
    @Override
    public String addCompteEpargne(CompteEpargne compteEpargne) {
        if (compteRepository.findById(compteEpargne.getCodeCompte()).isPresent()) {
            throw new RuntimeException("Compte deja existant");
        } else {
            compteRepository.save(compteEpargne);
            return "Votre Compte Epargne bien ajouter";
        }
    }

    @Transactional
    @Override
    public String verser(String codeCpte, double montant) {
        if (compteRepository.findById(codeCpte).isPresent()) {
            Optional<Compte> cp = compteRepository.findById(codeCpte);
            Versement v = new Versement(new Date(), montant, cp.get());
            operationRepository.save(v);
            cp.get().setSolde(cp.get().getSolde() + montant);
            compteRepository.save(cp.get());
            return "Your payment well done";
        } else {
            throw new ResourceNotFoundException("Error check your codeCompte or montant");
        }
    }

    @Transactional
    @Override
    public String withdrawal(String codeCpte, double montant) {
        Compte compte = getCompteById(codeCpte).get();
        double facilitesCaisse = 0;
        if (compte instanceof CompteCourant) {
            facilitesCaisse = ((CompteCourant) compte).getDecouvert();
            if (compte.getSolde() + facilitesCaisse < montant) throw new RuntimeException("Slode insuffisant");
        }
        Retrait retrait = new Retrait(new Date(), montant, compte);
        operationRepository.save(retrait);
        compte.setSolde(compte.getSolde() - montant);
        compteRepository.save(compte);
        return "Your withdrawal is well recorded";
    }

    @Transactional
    @Override
    public String transfer(String codeCompteRetrait, String codeCompteVersement, double montant) {
        if (codeCompteVersement == null) {
            throw new RuntimeException("Impossible : l'ID du destinataire pour ce transfere n'est pas valide");
        } else if (codeCompteRetrait.equals(codeCompteVersement)) {
            throw new RuntimeException("Impossible : On ne peut pas effectuer un virement dans le meme Compte");
        } else {
            withdrawal(codeCompteRetrait, montant);
            verser(codeCompteVersement, montant);
            return "Your transfer well recorded";
        }
    }

    @Transactional
    @Override
    public List<Operation> listOperation(String codeCpte) {
        return operationRepository.listOperationByCompte(codeCpte);
    }
}

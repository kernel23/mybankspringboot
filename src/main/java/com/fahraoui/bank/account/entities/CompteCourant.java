package com.fahraoui.bank.account.entities;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import java.util.Date;


@NoArgsConstructor
@Getter
@Setter
@Entity
@DiscriminatorValue("CC")
public class CompteCourant extends Compte {
    private double decouvert;

    public CompteCourant(String CodeC, Date dateCreation, double solde, Client client, double decouvert) {
        super(CodeC, dateCreation, solde, client);
        this.decouvert = decouvert;
    }
}

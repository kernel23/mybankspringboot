package com.fahraoui.bank.account.entities;

import lombok.AllArgsConstructor;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import java.util.Date;


@AllArgsConstructor
@Entity
@DiscriminatorValue("V")
public class Versement extends Operation {

    public Versement(Date dateOperation, double montant, Compte compte) {
        super(dateOperation, montant, compte);
    }
}

package com.fahraoui.bank.account.entities;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.*;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Collection;
import java.util.Date;

@Getter
@Setter
@EqualsAndHashCode
@NoArgsConstructor
@RequiredArgsConstructor
@Inheritance(strategy = InheritanceType.SINGLE_TABLE)
@DiscriminatorColumn(name = "TYPE_CPTE", discriminatorType = DiscriminatorType.STRING, length = 2)
@Entity
@Data
@Table(name = "comptes")
public abstract class Compte implements Serializable {

    @Id
    @NonNull
    private String codeCompte;
    @NonNull
    private Date dateCreation;
    @NonNull
    private double solde;

    @ManyToOne
    @JoinColumn(name = "CODE_CLI")
    @NonNull
    @JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
    private Client client;

    @OneToMany(mappedBy = "compte", orphanRemoval = true)
    @JsonBackReference
    private Collection<Operation> operations;
    ;
}

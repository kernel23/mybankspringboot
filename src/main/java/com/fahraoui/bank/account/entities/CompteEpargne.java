package com.fahraoui.bank.account.entities;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import java.util.Date;

@NoArgsConstructor
@Getter
@Setter
@Entity
@DiscriminatorValue("CE")
public class CompteEpargne extends Compte {
    private double taux;

    public CompteEpargne(String comteE, Date dateCreation, double solde, Client client, double taux) {
        super(comteE, dateCreation, solde, client);
        this.taux = taux;
    }
}

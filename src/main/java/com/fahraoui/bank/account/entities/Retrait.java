package com.fahraoui.bank.account.entities;

import lombok.NoArgsConstructor;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import java.util.Date;


@NoArgsConstructor
@Entity
@DiscriminatorValue("R")
public class Retrait extends Operation {

    public Retrait(Date dateOperation, double montant, Compte compte) {
        super(dateOperation, montant, compte);
    }
}

package com.fahraoui.bank.account.entities;

public enum RoleName {
    ROLE_USER,
    ROLE_ADMIN
}

package com.fahraoui.bank.account.entities;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.*;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;


@RequiredArgsConstructor
@NoArgsConstructor
@Getter
@Setter
@Inheritance(strategy = InheritanceType.SINGLE_TABLE)
@DiscriminatorColumn(name = "TYPE_OP", discriminatorType = DiscriminatorType.STRING, length = 1)
@Entity
@Data
@Table(name = "operations")
public abstract class Operation implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long numero;
    @NonNull
    private Date dateCreation;
    @NonNull
    private double montant;
    @ManyToOne
    @JoinColumn(name = "CODE_CPTE")
    @NonNull
    @JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
    private Compte compte;
}
